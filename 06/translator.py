# -*- coding: utf-8 -*-
"""
Contains the Translator.
"""
import logging
import os
from parser_ import Parser


class Translator(object):
    """
    Translates a Hack assembly file to a Hack machine code file.
    """
    HACK_FILE_EXT = '.hack'

    def __init__(self, input_fn, output_fn):
        self._input_fn = input_fn
        self._output_fn = output_fn
        self._input_file = None
        self._output_file = None
        self._ready = False

    def translate(self):
        """
        Does the translation itself.
        """
        if not self._ready:
            raise AssertionError('Wrong usage of Translator')

        parser = Parser()

        # first pass
        for line in self._input_file:
            parser.peek_line(line)
        self._input_file.seek(0)

        # parse and translate each line
        for line in self._input_file:
            logging.debug('Found line: %s', line.strip())
            cmd = parser.parse_line(line)
            if cmd is None:
                logging.debug('Skipping empty line')
                continue
            logging.debug('Command is: %r', cmd)
            opcode = cmd.assemble()
            self._output_file.write(opcode + os.linesep)

    def __enter__(self):
        self._ready = True
        self._input_file = open(self._input_fn, 'rb')
        self._output_file = open(self._output_fn, 'wb')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._ready = False
        try:
            self._input_file.close()
            self._input_file = None
        except:
            pass
        try:
            self._output_file.close()
            self._output_file = None
        except:
            pass

    @staticmethod
    def make_output_fn(input_fn):
        """
        Generates an output filename for a given input filename.

        @param input_fn: The given input filename.
        @return: The generated output filename.
        """
        return os.path.splitext(os.path.basename(input_fn))[0] + Translator.HACK_FILE_EXT

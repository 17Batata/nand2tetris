# -*- coding: utf-8 -*-
import argparse
import logging
import sys
from translator import Translator


VERSION = '0.1'


def setup_parser():
    """
    Sets up argument parser.

    @return: The parser.
    """
    parser = argparse.ArgumentParser()

    # add arguments
    parser.add_argument('file', type=str,
                        help='hack assembly program')
    parser.add_argument('-v', '--verbosity', action='count', default=0,
                        help='increase output verbosity')

    return parser


def setup_logging(verbosity):
    """
    Sets up logging facilities.

    @param verbosity: The level of verbosity (0-3).
    """
    logger = logging.getLogger()

    # choose level
    level = logging.ERROR
    if verbosity >= 3:
        level = logging.DEBUG
    elif verbosity == 2:
        level = logging.INFO
    elif verbosity == 1:
        level = logging.WARN
    logger.setLevel(level)

    # add console handler
    sh = logging.StreamHandler()
    sh.setLevel(level)

    # set formatter
    formatter = logging.Formatter('[%(levelname)s] %(message)s')
    sh.setFormatter(formatter)

    # finally
    logger.addHandler(sh)


def main():
    """
    The program entry point.

    @return: The return code of the program.
    """
    # setup parser and logging
    parser = setup_parser()
    args = parser.parse_args()
    setup_logging(args.verbosity)

    logging.info('Assembler v%s started', VERSION)

    # main logic
    try:
        input_fn = args.file
        output_fn = Translator.make_output_fn(input_fn)
        logging.info('Input file: %s', input_fn)
        logging.info('Output file: %s', output_fn)

        # Translator does all the work
        with Translator(input_fn, output_fn) as translator:
            translator.translate()
    except Exception, e:
        logging.exception(e)
        return 1

    logging.info('Finished successfully')

    return 0


if __name__ == '__main__':
    sys.exit(main())

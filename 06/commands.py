# -*- coding: utf-8 -*-
"""
Contains representations of Hack commands.
"""
from abc import ABCMeta, abstractmethod


class AssemblyCommand(object):
    """
    Metaclass of a assembly command.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def assemble(self):
        """
        Abstract.

        @raise NotImplementedError: Always.
        """

        raise NotImplementedError()

    @staticmethod
    def opcode(n):
        """
        Creates an opcode from an integer.

        @param n: The input integer.
        @return: The formatted opcode.
        """

        return '{0:016b}'.format(n)


class ACommand(AssemblyCommand):
    """
    Represents an A command.

    Contains only the value field.
    """
    def __init__(self, value):
        self.value = value

    def assemble(self):
        """
        Assembles the A command.

        @return: The assembled opcode.
        """

        return AssemblyCommand.opcode(self.value & 0x7FFF)

    def __repr__(self):
        return '%s(value=%d)' % (self.__class__.__name__, self.value)


class CCommand(AssemblyCommand):
    """
    Represents a C command.

    Contains the comp, dest and jump fields.
    """
    def __init__(self, comp, dest, jump):
        self.comp = comp
        self.dest = dest
        self.jump = jump

    def assemble(self):
        """
        Assembles the C command.

        @return: The assembled opcode.
        """
        self.comp = (self.comp & 0x007F) << 6
        self.dest = (self.dest & 0x0007) << 3
        self.jump &= 0x0007

        return AssemblyCommand.opcode(0xE000 | self.comp | self.dest | self.jump)

    def __repr__(self):
        return '%s(comp=%s, dest=%s, jump=%s)' % (self.__class__.__name__, self.comp, self.dest, self.jump)

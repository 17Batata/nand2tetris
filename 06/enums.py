# -*- coding: utf-8 -*-
"""
Contains some enums.
"""
from enum import Enum


class CommandType(Enum):
    """
    Represents a Hack command type.
    """
    A_COMMAND = 0
    C_COMMAND = 1
    L_COMMAND = 2


class CompType(Enum):
    """
    Represents the options of a comp field in a C command.
    """
    ZERO_2A = '0'
    ONE_3F = '1'
    MINUS_ONE_3A = '-1'
    D_0C = 'D'
    A_30 = 'A'
    M_70 = 'M'
    NOT_D_0D = '!D'
    NOT_A_31 = '!A'
    NOT_M_71 = '!M'
    MINUS_D_0F = '-D'
    MINUS_A_33 = '-A'
    MINUS_M_73 = '-M'
    D_PLUS_ONE_1F = 'D+1'
    A_PLUS_ONE_37 = 'A+1'
    M_PLUS_ONE_77 = 'M+1'
    D_MINUS_ONE_0E = 'D-1'
    A_MINUS_ONE_32 = 'A-1'
    M_MINUS_ONE_72 = 'M-1'
    D_PLUS_A_02 = 'D+A'
    D_PLUS_M_42 = 'D+M'
    D_MINUS_A_13 = 'D-A'
    D_MINUS_M_53 = 'D-M'
    A_MINUS_D_07 = 'A-D'
    M_MINUS_D_47 = 'M-D'
    D_AND_A_00 = 'D&A'
    D_AND_M_40 = 'D&M'
    D_OR_A_15 = 'D|A'
    D_OR_M_55 = 'D|M'


class DestType(Enum):
    """
    Represents the options of a dest field in a C command.
    """
    NULL_00 = 'null'
    M_01 = 'M'
    D_02 = 'D'
    MD_03 = 'MD'
    A_04 = 'A'
    AM_05 = 'AM'
    AD_06 = 'AD'
    AMD_07 = 'AMD'


class JumpType(Enum):
    """
    Represents the options of a jump field in a C command.
    """
    NULL_00 = 'null'
    JGT_01 = 'JGT'
    JEQ_02 = 'JEQ'
    JGE_03 = 'JGE'
    JLT_04 = 'JLT'
    JNE_05 = 'JNE'
    JLE_06 = 'JLE'
    JMP_07 = 'JMP'

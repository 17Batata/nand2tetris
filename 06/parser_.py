# -*- coding: utf-8 -*-
"""
Contains the Parser.
"""
from commands import ACommand, CCommand
from enums import CommandType, CompType, DestType, JumpType


class HackParserError(BaseException):
    """
    Indicates a Parser error.
    """
    pass


class Parser(object):
    """
    Parses a Hack assembly line.
    """
    def __init__(self):
        self.symbols = {}
        self._pc = 0
        self._next_var = 0x0010

        self._init_predefined_symbols()

    @staticmethod
    def decode_line(line):
        """
        Detects the current line's command type.

        @param line: The input line.
        @return: The command type.
        """
        # clean line
        line = Parser._clean_comments(line)
        line = Parser._clean_whitespaces(line)

        # empty line
        if len(line) == 0:
            return None, None

        # decode command type
        if len(line) > 1 and line[0] == '@':
            command_type = CommandType.A_COMMAND
        elif len(line) > 2 and line[0] == '(' and line[-1] == ')':
            command_type = CommandType.L_COMMAND
        else:
            command_type = CommandType.C_COMMAND

        return line, command_type

    def peek_line(self, line):
        """
        First pass, only takes care of labels.

        @param line: The line.
        """
        # decode command type
        line, command_type = Parser.decode_line(line)

        # empty line
        if line is None:
            return

        if command_type == CommandType.A_COMMAND:
            self._pc += 1
        elif command_type == CommandType.C_COMMAND:
            self._pc += 1
        elif command_type == CommandType.L_COMMAND:
            name = line[1:-1]
            if name in self.symbols:
                raise HackParserError('Found a label overriding another symbol: %s' % (name, ))
            else:
                self.symbols[name] = self._pc

    def parse_line(self, line):
        """
        Parses a Hack assembly line.

        @param line: Hack assembly line.
        @return: An appropriate Command subclass, None on empty line.
        @raise HackParserError: On invalid command type.
        """
        # decode command type
        line, command_type = self.decode_line(line)

        # empty line
        if line is None:
            return None

        # parse line
        if command_type == CommandType.A_COMMAND:
            cmd = self.parse_a_command(line)
        elif command_type == CommandType.C_COMMAND:
            cmd = Parser.parse_c_command(line)
        elif command_type == CommandType.L_COMMAND:
            cmd = None
        else:
            raise HackParserError('Invalid command type')

        return cmd

    def parse_a_command(self, line):
        """
        Parses an A command.

        @param line: A command assembly line.
        @return: Appropriate ACommand instance.
        """
        # referencing a symbol
        if not line[1].isdigit():
            name = line[1:]
            # referencing a var
            if name not in self.symbols:
                self.symbols[name] = self._next_var
                self._next_var += 1
            value = int(self.symbols[name])
        else:
            # value is literal
            try:
                value = int(line[1:])
            except ValueError:
                raise HackParserError('Invalid value in A command, must be numeric')
            if value < 0:
                raise HackParserError('Invalid value in A command, must be non-negative')
            if value != value & 0x7FFF:
                raise HackParserError('Invalid value in A command, must be 0-0x7FFF')

        return ACommand(value)

    @staticmethod
    def parse_c_command(line):
        """
        Parses an C command.

        @param line: C command assembly line.
        @return: Appropriate CCommand instance.
        """

        # split jump, default is null
        jump = 'null'
        parts = line.split(';')
        if len(parts) == 2:
            line, jump = parts
        elif len(parts) == 1:
            pass
        else:
            raise HackParserError('Invalid C command')

        # split to comp and dest
        parts = line.split('=')
        if len(parts) == 2:
            dest, comp = parts
        elif len(parts) == 1:
            dest = 'null'
            comp = parts[0]
        else:
            raise HackParserError('Invalid C command')

        # parse fields
        dest = Parser._parse_dest(dest)
        comp = Parser._parse_comp(comp)
        jump = Parser._parse_jump(jump)

        return CCommand(comp, dest, jump)

    def _init_predefined_symbols(self):
        self.symbols = {
            'SP': 0x0000,
            'LCL': 0x0001,
            'ARG': 0x0002,
            'THIS': 0x0003,
            'THAT': 0x0004,
            'SCREEN': 0x4000,
            'KBD': 0x6000,
        }

        # fill R0-R15 registers map
        for i in xrange(16):
            self.symbols['R%d' % (i, )] = i

    @staticmethod
    def _parse_comp(comp):
        try:
            return int(CompType(comp).name.split('_')[-1], 16)
        except ValueError:
            raise HackParserError('Can\'t parse comp: %s' % (comp, ))

    @staticmethod
    def _parse_dest(dest):
        try:
            return int(DestType(dest).name.split('_')[-1], 16)
        except ValueError:
            raise HackParserError('Can\'t parse dest: %s' % (dest, ))

    @staticmethod
    def _parse_jump(jump):
        try:
            return int(JumpType(jump).name.split('_')[-1], 16)
        except ValueError:
            raise HackParserError('Can\'t parse jump: %s' % (jump, ))

    @staticmethod
    def _clean_comments(line):
        comment_pos = line.find('//')
        if comment_pos > -1:
            line = line[:comment_pos]

        return line

    @staticmethod
    def _clean_whitespaces(line):
        line = line.strip()
        line = line.replace(' ', '')
        line = line.replace('\t', '')

        return line

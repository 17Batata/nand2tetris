// This file is part of the materials accompanying the book 
// "The Elements of Computing Systems" by Nisan and Schocken, 
// MIT Press. Book site: www.idc.ac.il/tecs
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, 
// the screen should be cleared.

    @24576
    D=A
    @end
    M=D
(LOOP)
    // if KDB != 0 goto BLACK
    // else        goto WHITE
    @KBD
    D=M
    @BLACK
    D;JNE
    @WHITE
    D;JMP
    @LOOP
    0;JMP
(BLACK)
    // pixel = 0xFFFF
    @pixel
    D=0
    M=!D
    // goto WORK
    @WORK
    0;JMP
(WHITE)
    // pixel = 0x0000
    @pixel
    M=0
    // goto WORK
    @WORK
    0;JMP
(WORK)
    // i = SCREEN
    @SCREEN
    D=A
    @i
    M=D
(WORKLOOP)
    // if i == end goto LOOP
    @i
    D=M
    @end
    A=M
    D=D-A
    @LOOP
    D;JEQ
    // RAM[i] = pattern
    @pixel
    D=M
    @i
    A=M
    M=D
    // i = i + 1
    D=A
    D=D+1
    @i
    M=D
    // goto WORKLOOP
    @WORKLOOP
    0;JMP
